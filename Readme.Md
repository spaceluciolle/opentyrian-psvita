# OpenTyrian for ps vita
## About OpenTyrian
OpenTyrian is an open-source port of the DOS game Tyrian.

Tyrian is an arcade-style vertical scrolling shooter.  The story is set
in 20,031 where you play as Trent Hawkins, a skilled fighter-pilot employed
to fight MicroSol and save the galaxy.

Tyrian features a story mode, one- and two-player arcade modes, and networked
multiplayer.

## Requirement
- A ps vita on 3.60/3.65/3.68 firmware with henkaku installed
- Tyrian 2.1 assets [download them here](http://camanis.net/tyrian/tyrian21.zip)
  - put them on ux0:opentyr/data 
